# Near Future Solar :: Change Log

* 2020-1229: 1.3.1 (Nertea) for KSP 1.11.0
	+ Fixed cargo parts for KSP 1.10
* 2020-1223: 1.3.0 (Nertea) for KSP 1.11.0
	+ KSP 1.11
	+ Set many panels as useable in inventories in various ways
* 2020-0803: 1.2.3 (Nertea) for KSP 1.10.1
	+ Marked for KSP 1.1.0
	+ Updated B9PartSwitch to 2.17.0
	+ Updated ModuleManager to 4.1.4
	+ Added Portugese localization (Rib3iroJr)
* 2020-0526: 1.2.2 (Nertea) for KSP 1.9.1
	+ Updated B9PartSwitch to 2.16.0
	+ Updated Chinese translation (tinygrox)
	+ Enumerated all panel objects on Hub panels, reducing warnings in the log
* 2020-0222: 1.2.1 (Nertea) for KSP 1.9.0
	+ Changed the attach node on the curved panels to behave better with surface attachemnt (sorry, this will affect craft)
* 2020-0220: 1.2.0 (Nertea) for KSP 1.9.0
	+ Marked compatible with KSP 1.9+
	+ Updated bundled ModuleManager to 4.1.3
	+ B9PartSwitch is now bundled and required
	+ Switchable cell feature implemented
		- All previous Advanced panels can now toggle between Basic, Advanced and Concentrator cells with different looks and performance. Advanced represents previous baseline, Concentrator increases power per unit area but weighs more, Basic decreases power per unit area, but weighs less.
		- All previous Blanket panels can now toggle between Basic Blanket, and Advanced Blanket cells with different looks and performance. Advanced represents previous baseline, Basic decreases power per unit area, but weighs less.
		- All previous Concentrator panels can now toggle between Basic, Advanced and Concentrator cells with different looks and performance. Concentrator represents previous baseline, Advanced decreases power per unit area but weighs less, Basic further decreases power per unit area, but weighs least.
	+ Some panel names were adjusted to be clearer
	+ Fixed an issue where most or all normal maps in the mod had some problems
	+ Fixed an issue with the Solo panel's suncatcher not extending with the rest of the model
* 2019-1107: 1.1.0 (Nertea) for KSP 1.8.1
	+ KSP 1.8.x
	+ Plugin recompiled against .NET 4.5
	+ Updated bundled ModuleManager to 4.1.0
	+ REMOVED ALL DEPRECATED OLD SOLAR PANEL PARTS
	+ Updated MM patches to have correct pass specifications when needed
	+ Added new panel models
		- K3x75-A 1x1 Advanced Photovoltaic Panels: Small Advanced 3.75 kW array based on MESSENGER panels. Not retractable
		- K3-A 3x1 Advanced Photovoltaic Panels: Small Advanced 3 kW array based on Worldview-3 panels. Not retractable
		- K4-AFR 1x2 Advanced Photovoltaic Panels: Small Advanced 4 kW array based on Worldview-4 panels. Does not track sun
		- K10-AFR 3x1 Advanced Photovoltaic Panels: Small Advanced 10 kW array based on Hayabusa-2 panels. Does not track sun
		- K0f7-AFR 1x1 Advanced Photovoltaic Panels: Small Advanced 0.7 kW array based on Ikonos-1 panels. Does not track sun
* 2019-0910: 1.0.5 (Nertea) for KSP 1.7.3
	+ Fixed an issue with the Halo panel colliders
* 2019-0821: 1.0.4 (Nertea) for KSP 1.7.3
	+ Fixed rogue lights on Megalador and Halo panels
	+ Updated Chinese translations (6DYZBX)
* 2019-0709: 1.0.3 (Nertea) for KSP 1.7.0
	+ Fixed CTT location of small truss solar array
	+ Moved truss solar arrays to Advanced Solar Tech node in CTT mode
* 2019-0521: 1.0.2 (Nertea) for KSP 1.7.0
	+ Some fixed manufacturers (Sool3)
	+ Added Russian localization (Sool3)
* 2019-0520: 1.0.1 (Nertea) for KSP 1.7.0
	+ Fixed OKEB-15 and -45 being retractable
	+ Fixed OKEB-150 not being retractable
	+ Updated Spanish translation
	+ Fixed power output of Ares panels
* 2019-0512: 1.0.0 (Nertea) for KSP 1.7.0
	+ KSP 1.7.x
	+ Final content update
	+ Updated ModuleManager to 4.0.2
	+ Soft-deprecated all old solar panel parts: you have 2-3 months before they are removed PERMANENTLY (this was necessary due to extensive model changes)
	+ New part configs and names introduced * Complete artistic redo
		- Division of Basic, Advanced, Blanket, Concentrator panels is now visually clear     * Use of reflective shader to improve visual fidelity
		- Consistent texel density for all components
		- Visual continuity with Restock project
	+ Panel naming convention has been clarified:
		- Non-blanket deployable panel nomenclature is now K{EC Output}-{Suffix}, where Suffix is composed of A (advanced), R (retractable) and/or K (concentrating) as appropriate
		- Blanket nomenclature is OKEB-{EC Output} with an R suffix if retractable
		- Circular nomenclature is NIV-{EC Output} with an R suffix if retractable
		- Most larger panels got new nicknames
		- Removed certain tags from un-retractable panels to remove them from searches
	+ Manufacturer naming has been tweaked and consolidated into two manufacturers
	+ Many descriptions were rewritten to account for changes
	+ Several replaced/significantly modified models:
		- K7-A/K7-AR 1x4 panels have been replaced with a modern Dragon 1 style
		- K22-A, K15-A panels are now physically larger (previously they were too small for their power output)
		- K55 Megalador has a completely different model with a different closed footprint
		- OKEB-100 has a completely different model with a different closed and open footprint based off a Deep Space Gateway concept illustration
		- OKEB-500 has a completely different model with a different closed and open footprint based off NASA DRM5 SEP-CHEM option
		- OKEB-75-R has a completely different model with a different closed and open footprint, and is now fully shrouded.
		- NIV-30 has been reworked to be visually similar to Dragon 2 wraparound panels
		- NIV-18 has been reworked to be visually similar to Dragon 2 wraparound panels but in a taller footprint. Renamed to NIV-45.
		- Static truss panels can now be switched between truss and flat versions for flush attachment options
	+ Balance changes:
		- Concentrator arrays have new cost/mass tier: even more expensive, slightly more massive, best output/area
		- OKEB-125 panel can now be retracted, power output updated to 150 kw (also retitled OKEB-150 as a result)
		- K90 'Trio' and K30 'Solo' are now Blanket style arrays from a balance PoV, and are retitled as OKEB-45 and OKEB-15. They can no longer be retracted.
		- YF-8 Circular Solar Array is now Blanket style from a balance PoV (higher mass efficiency, lower cost, worse output/area), and is retitled as OKEB-4R with 4 kW output
		- Mass, cost and unlock cost were tuned for many panels
		- Crash tolerances were normalized by type: Advanced     * Shrouded panels now have higher thermal tolerances (2000K) and Blanket panels now have smaller thermal tolerances (1000K). Other panels normalized to 1200K.
		- Tech location tuned for all parts
	+ New panels:
		- K2-A 1x2 Advanced Photovoltaic Panels: Small Advanced 2 kW array based on MRO panels
		- K4-A 3x1 Advanced Photovoltaic Panels: Small Advanced 4 kW array based on TDRSS Gen 1 panels
		- K20-C 1+2+1 Concentrating Photovoltaic Panels: Medium Concentrating 20 KW array based on JUICE panels
		- K40-AR 'Oar' Advanced Photovoltaic Array: Medium Advanced 40 kW sized retractable array
		- KX-STAT-2 Advanced Photovoltaic Truss: Single-panel version of the KX-STAT series (2kW)
		- OKEB-25-R 'Starship' Blanket Photovoltaic Array: Small Blanket 25 kW panels based off the SpaceX Starship panels. Fully shrouded.
		- OKEB-250 'Ares' Blanket Photovoltaic Array: Huge Blanket 250 kW panels based off NASA DST concept art
		- IX-A 3x1 Advanced Photovoltaic Panels: Small Advanced 4 kW array based on TDRSS Gen 1 panels
	+ Curved solar panels now extend 10x faster in the VAB/SPH
	+ Curved solar panels now update their symmetric counterparts when deploying/retracting in the VAB/SPH
* 2019-0121: 0.8.15 (Nertea) for KSP 1.6.1
	+ German translation from Three_Pounds
	+ Fixed missing bulkhead profiles (thanks Streetwind)
* 2019-0117: 0.8.14 (Nertea) for KSP 1.6.1
	+ KSP 1.6.x
	+ Updated MM to 3.1.3
	+ License change for code and configs to MIT
* 2018-1105: 0.8.13 (Nertea) for KSP 1.4.4
	+ KSP 1.5.x
	+ Updated MM to 3.1.0
	+ Removed MiniAVC distribution
* 2018-0807: 0.8.12 (Nertea) for KSP 1.4.2
	+ KSP 1.4.5
	+ Updated bundled MM version
	+ Rebuilt project for new version
	+ Widened KSP min/max specification
* 2018-0503: 0.8.11 (Nertea) for KSP 1.4.2
	+ KSP 1.4.3
	+ Switched versioning to mix/max specification
* 2018-0411: 0.8.10 (Nertea) for KSP 1.4.2
	+ Fixed .version file issue
* 2018-0410: 0.8.9 (Nertea) for KSP 1.3.1
	+ KSP 1.4.2 rebuild
	+ Updated MiniAVC to 1.2.0.1
	+ Fixed sun distance being hardcoded (jsolson)
* 2017-1128: 0.8.8 (Nertea) for KSP 1.3.1
	+ Fixed scaling of large fixed solar array
	+ Adjusted suncatcher location of Trio solar array
	+ Reexport of OKEB-75 solar array, might help FAR problem, might not.
* 2017-1013: 0.8.7 (Nertea) for KSP 1.3.0
	+ KSP 1.3.1
	+ Dependency updates
* 2017-0731: 0.8.6 (Nertea) for KSP 1.3.0
	+ Improved chinese translation
	+ Fixed wrong version of MM
* 2017-0726: 0.8.5 (Nertea) for KSP 1.3.0
	+ Moved collider hierarchy for OKEB-75 Solar array (fixes collider not detaching)
	+ Chinese translation courtesy of forum user DY_ZBX
* 2017-0630: 0.8.4 (Nertea) for KSP 1.3.0
	+ Fixed a rogue debug logging call
* 2017-0626: 0.8.3 (Nertea) for KSP 1.3.0
	+ Spanish Translation
* 2017-0620: 0.8.2 (Nertea) for KSP 1.3.0
	+ Fixed specular exponent of T22 1x5 Concentrating Photovoltaic Panels being applied to the whole panel and not just the cells
	+ Fixed specular exponent of T14 1x5 Concentrating Photovoltaic Panels being applied to the panel instead of the cells
	+ Normalized specular exponent of blanket panels
	+ Fixed attach orientation and scaling of PX-STAT 1x2 Photovoltaic Truss
	+ Fixed electricity generation of all curved solar panels
	+ Fixed mixed up names of NIV-10 and NIV-30 Curved Solar Arrays
	+ Fixed typos in a few panel names
* 2017-0616: 0.8.1 (Nertea) for KSP 1.3.0
	+ Hotfix missing texture
* 2017-0616: 0.8.0 (Nertea) for KSP 1.2.2
	+ KSP 1.3
	+ Updated bundled MM to 2.8.0
	+ Converted all part name/descriptions to localization system
	+ Rebalanced solar panel masses and costs for more consistency
	+ Removed retractable part search tag from parts that are not retractable
	+ Renamed some solar panels to increase consistency and ease of inter-panel comparison
	+ Reworked manufacturer names to reduce the number of new manufacturers
	+ Adjusted position of suncatcher transforms on SOL-A, NIV-3 and NIV-16 panels
	+ K7R 1x4 Concentrating Photovoltaic Panels are no longer retractable
	+ Adjusted shaders for many panels to improve looks
	+ Added K7R 1x4 Concentrating Photovoltaic Panels and K5R 1x3 Concentrating Photovoltaic Panels, variants of two current panels with added shrouds and retractability
	+ Added SOL-B Expanding Curved Solar Array (3.75m to 10m expanding curved solar array)
	+ Added OKEB-125 Blanket Solar Array (based on NASA asteroid tug concept)
	+ Added OKEB-75 Blanket Solar Array (based on SpaceX ITS solar array concept)
* 2017-0123: 0.7.2 (Nertea) for KSP 1.2.1
	+ Marked for KSP 1.2.2
	+ Updated MM to 2.7.5
	+ Standardized manufacturer names
* 2016-1118: 0.7.1 (Nertea) for KSP 1.2
	+ Marked for KSP 1.2.1
	+ Updated MM to 2.7.4
* 2016-1021: 0.7.0 (Nertea) for KSP 1.1.3
	+ KSP 1.2
	+ Small tweak to plugin code for TweakScale and for 1.2
	+ Moved all panels to the new Electrics category
	+ Minor tweak to plugin that will help TweakScale work on curved panels if it needs to
	+ Updated bundled MM version
* 2016-0626: 0.6.2 (Nertea) for KSP 1.1.2
	+ KSP 1.1.3
	+ Updated bundled MM version
* 2016-0511: 0.6.1 (Nertea) for KSP 1.1
	+ KSP 1.1.2
	+ Updated bundled MM version
	+ Fixed curved panels not being retractable in the editor
* 2016-0422: 0.6.0 (Nertea) for KSP 0.7.3
	+ No changelog provided
